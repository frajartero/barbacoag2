== Lista de bebidas

// Separadas en con/sin alcohol
// Ordenadas por orden alfabético

=== Con alcohol

* Beffeater
* Cerveza
* Johnnie Walker Blue Label (elixir)

=== Sin alcohol

* Agua
* Cerveza sin
* Coca Cola
* Fanta de Naranja
* Pepsi Zero
* Seven Up
