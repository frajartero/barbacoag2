== ﻿Lista de comida

// Ordenada por orden alfabético en cada apartado

=== Aperitivos

* Ensaladilla Rusa
* Guacamole
* Patatas fritas
* Pepinillos fritos

=== Platos principales

* Cabezada
* Chuletas de cabeza
* Costillas de cerdo
* Paella de marisco
* Tortilla de patatas

=== Postres

* Arroz con leche
* Natillas
* Yogur de fresa
